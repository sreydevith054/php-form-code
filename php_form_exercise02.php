<?php
  $lastname=$firstname=$email=$sex="";
  $tel=$birth=null;
  $lastname=$_POST['lastname']; $firstname=$_POST['firstname'];
  $sex=$_POST['sex'];
  $email=$_POST['email'];
  $tel=$_POST['tel'];
  $birth=$_POST['birth'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="stylesheet.css">
  <title>php form:</title>
</head>
<body>
  <h2>លំហាត់ទី០២:&nbsp;&nbsp;ទម្រង់form និង assign តម្លៃ</h2>
  <form action="php_form_exercise02.php" method="POST" style="width:50%;">
    <fieldset>
      <legend><b>ចូលបំពេញពត៌មានរបស់អ្នក:</b></legend>
      <br>
      <label for="lastname">នាម:
      <input type="text" name="lastname" value="<?php echo $lastname ?> ">
      </label>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <label for="firstname">គោត្តនាម:
      <input type="text" name="firstname" value="<?php echo $firstname ?> ">
      </label>
      <br><br>
      <p>ភេទ:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <label for="sex">មិនបញ្ជាក់</label>
      <input type="radio" name="sex" value="none">
      <label for="sex">ប្រុស</label>
      <input type="radio" name="sex" value="male" id="male">
      <label for="sex">ស្រី</label>
      <input type="radio" name="sex" value="female" id="female">
      </p>
      <br>
      <label for="birth">ថ្ងៃខែឆ្នាំកំណើត:</label>
      <input type="date" name="birth" value="<?php echo $birth ?>">
      <label for="email">អ៊ីម៉ែល:</label>
      <input type="email" name="email" value="<?php echo $email ?>">
      <br><br>
      <label for="tel">លេខទូរសព្ទ:</label>
      <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="number" name="tel" value="<?php echo $tel ?>">
      <br><br>
          &nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="submit">
    </fieldset>
  </form>

  <?php
    echo "នាម:  ". $lastname ."<br>";
    echo "គោត្តនាម:  ". $firstname ."<br>";
    echo "ភេទ:  ";
    if($sex=="male"){
      echo "ប្រុស";
    }else if($sex=="female"){
      echo "ស្រី";
    }else{
      echo "មិនបញ្ជាក់";
    }
    echo "<br>";
    echo "ថ្ងៃខែឆ្នាំកំណើត:  ".$birth ."<br>";
    echo "អ៊ីម៉ែល:  " . $email ."<br>";
    echo "ទូរសព្ទលេខ:  " . $tel ."<br>";
  ?>
</body>
</html>
